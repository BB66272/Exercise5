/*Classes within a package can access classes and members declared with default*/
package default; 
/*Class MyClass has 1 field with method package add and inside has two integers with the name of a and b. After it has return type and it has a sum.*/
public class MyClass {

public int add(int a, int b){ return a + b;

}
/* It takes two string and returs their values by equal them with each others.*/

public String add(String a, String b){ return a + b;

}
/*It is needed for substract method. Usually it,s sum with negative numbers and it returns after it values. Has also two integers.*/

public int sub(int a, int b){ return a - b;

}	
/*Mult is a multiplication for sum repeat several time. After it returns integers a and b.*/

public int mult(int a, int b){ return a * b;

}
/*This is a method of dividing. After it returns values of integers.*/

public int div(int a, int b){ return a / b;
}
/*Its for non negatives integers, we can use operator ’%’. Returns remainder of operators.*/

public int mod(int a, int b){ return a%b;



/*As a result its Hello, in string there is a parameter inside.*/
}

public String sayHello(String name){ return "Hello" + name;
}

}
